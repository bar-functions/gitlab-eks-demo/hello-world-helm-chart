import os
import yaml 

print("Starting")
time.sleep(10)

name = os.environ.get("USERNAME")
password = os.environ.get("PASSWORD")
print(name)
print(password)

def write(key, value):
    with open('secret.yaml', 'r') as file:
        data = yaml.safe_load(file) or {}
    data[key] = value
    with open('secret.yaml','w') as file:
        yaml.dump(data, file) 

write(key='username', value=name)
write(key='password', value=password)
print("End")
